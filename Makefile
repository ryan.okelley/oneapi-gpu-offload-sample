FC = ifx

FC_FLAGS = -qopenmp -fopenmp-targets=spir64 -o
SOURCE = matrix_multiply.f90
EXE_NAME = matrix_multiply

build:
	$(FC) $(FC_FLAGS) $(EXE_NAME) $(SOURCE)

submit-build:
	qsub -l nodes=1:gpu:ppn=2 -d . build.sh

run:
	./$(EXE_NAME)

submit-run:
	qsub -l nodes=1:gpu:ppn=2 -d . run.sh

watch:
	watch -n 1 qstat -n -1

clean-output:
	rm ./build.sh.e*; rm ./build.sh.o*; rm ./run.sh.e*; rm ./run.sh.o*;

clean:
	rm matrix_multiply