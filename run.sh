#!/bin/bash

export OMP_TARGET_OFFLOAD=DEFAULT
export LIBOMPTARGET_DEBUG=1
export LIBOMPTARGET_PLUGIN=OPENCL
export LIBOMPTARGET_PLUGIN_PROFILE=T
export LIBOMPTARGET_DEBUG=0

echo
echo "Starting CPU run..."
echo
export LIBOMPTARGET_DEVICETYPE=CPU
make run
echo "$? was the exit code"

echo
echo "Starting GPU run..."
echo
export LIBOMPTARGET_DEVICETYPE=GPU
make run
echo "$? was the exit code"

echo
echo "run.sh finished."
echo