#!/bin/bash

source /opt/intel/oneapi/setvars.sh > /dev/null 2>&1

echo
echo "Building..."
echo

make build

echo
echo "build.sh script finished."
echo