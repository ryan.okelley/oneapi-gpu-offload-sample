program matrix_multiply
    use omp_lib
    implicit none
    integer, parameter :: N=2000
    integer :: i, j, k
    real, allocatable, dimension(:,:) :: a, b, c

    allocate( a(N,N), b(N,N), c(N,N) )

    ! Initialize the arrays to zero goes here

    print *, "Starting matrix multiplication"

    !$omp target map(to: a, b) map(tofrom: c)
    !$omp parallel do
    do j=1,N
        do i=1,N
            do k=1,N
                c(i,j) = c(i,j) + a(i,k) * b(k,j)
            enddo
        enddo
    enddo
    !$omp end parallel do
    !$omp end target
   
    print *, "Finished matrix multiplication!"

end program matrix_multiply