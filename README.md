# OpenMP Matrix Multiplication Example
A trivial example of using OpenMP to offload to GPU from Fortran.

## Windows
Open a oneAPI command prompt and run `./build.ps1` to build and run the app with CPU and then GPU offload to compare the results. If you don't have an Intel GPU on your system you will (probably) get two CPU runs.

## Intel DevCloud
Run `make build` to build locally or `make submit-build` to build in a compute node. Run `make submit-run` to run the app on a compute node and `make watch` to watch the job queue. The results will appear in this folder as `run.sh.o*` and `run.sh.o*`.