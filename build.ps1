# Run this in the oneAPI command prompt environment
[CmdletBinding()]
param (
    [Parameter()]
    [switch] $SkipBuild
)

if (-not $SkipBuild) {
    Write-Host "Building"
ifx /Qopenmp /Qopenmp-targets:spir64 .\matrix_multiply.f90
}

$env:OMP_TARGET_OFFLOAD = 'DEFAULT'
$env:LIBOMPTARGET_DEBUG = '1'
$env:LIBOMPTARGET_PLUGIN = 'OPENCL'
$env:LIBOMPTARGET_PLUGIN_PROFILE = 'T'
$env:LIBOMPTARGET_DEBUG='0'

Write-Host "Starting CPU run..."
$env:LIBOMPTARGET_DEVICETYPE = 'CPU'
./matrix_multiply.exe

Write-Host "Starting GPU run..."
$env:LIBOMPTARGET_DEVICETYPE = 'GPU'
./matrix_multiply.exe
